'use strict'

const menuToggle = document.querySelector('.menu-toggle');
const bxMenu = document.querySelector('.bx-menu');
const bxX = document.querySelector('.bx-x');

const navBar = document.querySelector('.navbar');

// --- open menu ---

bxMenu.addEventListener('click', (e)=> {
    if(e.target.classList.contains('bx-menu')){
        navBar.classList.add('show-navbar');
        bxMenu.classList.add('hide-bx');
        bxX.classList.add('show-bx');
    }
})

// --- close menu ---

bxX.addEventListener('click', (e)=> {
    if(e.target.classList.contains('bx-x')){
        navBar.classList.remove('show-navbar');
        bxMenu.classList.remove('hide-bx');
        bxX.classList.remove('show-bx');
    }
})

// Navbar shirink
window.addEventListener('scroll', function() {
    const header = document.querySelector('header');
    header.classList.toggle('sticky', window.scrollY > 50)
})


let modeBtn = document.getElementById("dark-light");

modeBtn.addEventListener("click", function () {
  if (document.body.className != "light") {
    this.firstElementChild.src = "img/light.svg";
  } else {
    this.firstElementChild.src = "img/Dark.svg";
  }
  document.body.classList.toggle("light");
});

document.addEventListener('contextmenu', (event) => event.preventDefault());

document.addEventListener('keydown', (event) =>{
  if (event.keyCode === 123, 'I' || (event.ctrlKey && event.altKey && event.shiftKey)) {
    event.preventDefault();
  }
})

AOS.init({
  duration: 3000,
  once: true,
});


$(".owl-carousel-1").owlCarousel({
  loop: true,
  nav: true,
  dots: true,
  margin: 20,
  responsiveClass: true,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 2,
    },
    1000: {
      items: 3,
    },
  },
});
